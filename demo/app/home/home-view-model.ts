import { Observable } from "tns-core-modules/data/observable";
import * as phone from 'nativescript-phone';

export class HomeViewModel extends Observable {
    constructor() {
        super();
    }

    sendSMS() {
      phone.sms(['3212015043'], 'Hello').then(
        (result) => {
         console.log(result);
        },
        (error) => {
          console.error('Error: ', error);
        },
      );
    }
}
